using System.IO;
using LibVLCSharp.Shared;

namespace SpriteManager.Models;

public class SoundFx : LibVLC
{
    private string File { get; }
    private Media Media { get; }
    private MediaPlayer MediaPlayer { get; }
    
    public SoundFx(string file)
    {
        File = Path.GetFullPath(file);
        Media = new Media(this, File);
        MediaPlayer = new MediaPlayer(Media);
    }
    
    public void Play() => MediaPlayer.Play();
    
    public void Stop() => MediaPlayer.Stop();
    
    public VLCState State => MediaPlayer.State;
}