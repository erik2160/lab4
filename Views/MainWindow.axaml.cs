using System;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Input;
using LibVLCSharp.Shared;
using SpriteManager.Models;

namespace SpriteManager.Views;

public partial class MainWindow : Window
{
    private SoundFx? SoundFx { get; set; }
    public MainWindow()
    {
        InitializeComponent();
        Core.Initialize();
        BuildSound();
        Sprite.Margin = new Thickness(295, 225, 0, 0);
    }

    private void BuildSound()
    {
        SoundFx = new SoundFx("../Assets/button-hover-sfx.mp3");
    }

    private void Screen_OnKeyDown(object? sender, KeyEventArgs e)
    {
        if (SoundFx?.State == VLCState.Playing)
        {
            return;
        }
        
        SoundFx?.Stop();

        if (e.Key == Key.Left)
        {
            SoundFx?.Play();
            Sprite.Margin = new Thickness(Sprite.Margin.Left - 25, Sprite.Margin.Top, 0, 0);
        }
        else if (e.Key == Key.Right)
        {
            SoundFx?.Play();
            Sprite.Margin = new Thickness(Sprite.Margin.Left + 25, Sprite.Margin.Top, 0, 0);
        }
        
        else if (e.Key == Key.Up)
        {
            SoundFx?.Play();
            Sprite.Margin = new Thickness(Sprite.Margin.Left, Sprite.Margin.Top - 40, 0, 0);
        }
    }

    private void Screen_OnKeyUp(object? sender, KeyEventArgs e)
    {
        if (e.Key == Key.Up)
        {
            Sprite.Margin = new Thickness(Sprite.Margin.Left, Sprite.Margin.Top + 40, 0, 0);
        }
    }
}